package com.example.matthew.bolix;
import static com.example.matthew.bolix.HoneycombView.cellSet;
import static com.example.matthew.bolix.HoneycombView.winner;

/**
 * Created by Matthew on 11/15/2016.
 */

public class winChecker {
    public winChecker(int col, int row, int newCellColor) {
        // Check color above vertical
        if (!(row - 6 < 1)) {
            if (cellSet[col][row - 2] == newCellColor) {
                if (cellSet[col][row - 4] == newCellColor) {
                    if (cellSet[col][row - 6] == newCellColor) {
                        if (cellSet[col][row - 8] == newCellColor) {
                            winner = true;
                        }
                    }
                }
            }
        }
        // Check color below vertical
        if (!(row + 6 > 19)) {
            if (cellSet[col][row + 2] == newCellColor) {
                if (cellSet[col][row + 4] == newCellColor) {
                    if (cellSet[col][row + 6] == newCellColor) {
                        if (cellSet[col][row + 8] == newCellColor) {
                            winner = true;
                        }
                    }
                }
            }
        }
        // up left diagonally
        if(row % 2 == 0) { //even
            if (!(col - 2 < 0 || row - 3 < 0)) {
                if (cellSet[col - 1][row - 1] == newCellColor) {
                    if (cellSet[col - 1][row - 2] == newCellColor) {
                        if (cellSet[col - 2][row - 3] == newCellColor) {
                            if (cellSet[col - 2][row - 4] == newCellColor) {
                                winner = true;
                            }
                        }
                    }
                }
            }
        } else { //odd row
            if (!(col - 1 < 0 || row - 3 < 0)) {
                if (cellSet[col][row - 1] == newCellColor) {
                    if (cellSet[col - 1][row - 2] == newCellColor) {
                        if (cellSet[col - 1][row - 3] == newCellColor) {
                            if (cellSet[col - 3][row - 4] == newCellColor) {
                                winner = true;
                            }
                        }
                    }
                }
            }
        }

        // down left diagonally
        if(row % 2 == 0) { //even
            if (!(col - 2 < 0 || row + 3 > 19)) {
                if (cellSet[col - 1][row + 1] == newCellColor) {
                    if (cellSet[col - 1][row + 2] == newCellColor) {
                        if (cellSet[col - 2][row + 3] == newCellColor) {
                            if (cellSet[col - 2][row + 4] == newCellColor) {
                                winner = true;
                            }
                        }
                    }
                }
            }
        } else { //odd row
            if (!(col - 1 < 0 || row + 3 > 19)) {
                if (cellSet[col][row + 1] == newCellColor) {
                    if (cellSet[col - 1][row + 2] == newCellColor) {
                        if (cellSet[col - 1][row + 3] == newCellColor) {
                            if (cellSet[col - 2][row + 4] == newCellColor) {
                                winner = true;
                            }
                        }
                    }
                }
            }
        }

        // up right diagonally
        if(row % 2 == 0) { //even
            if (!(col + 1 > 5 || row - 3 < 0)) {
                if (cellSet[col][row - 1] == newCellColor) {
                    if (cellSet[col + 1][row - 2] == newCellColor) {
                        if (cellSet[col + 1][row - 3] == newCellColor) {
                            if (cellSet[col + 2][row - 4] == newCellColor) {
                                winner = true;
                            }
                        }
                    }
                }
            }
        } else { //odd row
            if (!(col + 2 > 5 || row - 3 < 0)) {
                if (cellSet[col + 1][row - 1] == newCellColor) {
                    if (cellSet[col + 1][row - 2] == newCellColor) {
                        if (cellSet[col + 2][row - 3] == newCellColor) {
                            if (cellSet[col + 2][row - 4] == newCellColor) {
                                winner = true;
                            }
                        }
                    }
                }
            }
        }

        // down right diagonally
        if(row % 2 == 0) { //even
            if (!(col + 1 > 5 || row + 3 > 19)) {
                if (cellSet[col][row + 1] == newCellColor) {
                    if (cellSet[col + 1][row + 2] == newCellColor) {
                        if (cellSet[col + 1][row + 3] == newCellColor) {
                            if (cellSet[col + 2][row + 4] == newCellColor) {
                                winner = true;
                            }
                        }
                    }
                }
            }
        } else { //odd row
            if (!(col + 2 > 5 || row + 3 > 19)) {
                if (cellSet[col + 1][row + 1] == newCellColor) {
                    if (cellSet[col + 1][row + 2] == newCellColor) {
                        if (cellSet[col + 2][row + 3] == newCellColor) {
                            if (cellSet[col + 2][row + 4] == newCellColor) {
                                winner = true;
                            }
                        }
                    }
                }
            }
        }
    }
}
