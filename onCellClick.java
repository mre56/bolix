package com.example.matthew.bolix;

import java.util.ArrayList;

import static com.example.matthew.bolix.MainActivity.honey;
/**
 * Created by Matthew on 11/15/2016.
 */

public class onCellClick {
    protected static boolean turn = true; // true = white, flase = black
    protected static boolean sandwichTurn = false;
    protected static ArrayList<int[]> sandwich = new ArrayList<int[]>();

    public onCellClick(int column, int row) {
        if (honey.getCell(column, row) == 0 && !sandwichTurn) {
            if (turn) {
                honey.setCell(column, row, 1); //white
            } else {
                honey.setCell(column, row, 2); //black
            }
            turn = !turn;
        } else if ((honey.getCell(column, row) == 1 && turn || honey.getCell(column, row) == 2 && !turn) && sandwichTurn) {
            for(int[] c: sandwich) {
                if(c[0] == column && c[1] == row){
                    honey.setCell(column, row, 3);
                    sandwichTurn = false;
                    sandwich.clear();
                    break;
                }
            }
        }
    }
    public static ArrayList getSandwich() {
        return sandwich;
    }
}
