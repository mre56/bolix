package com.example.matthew.bolix;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import static com.example.matthew.bolix.MainActivity.flippy;
import static com.example.matthew.bolix.MainActivity.honey;
import static com.example.matthew.bolix.MainActivity.initHoney;
import static com.example.matthew.bolix.MainActivity.toolbar;

/**
 * Created by Matthew on 11/8/2016.
 */

public class twoPlayerGame {

    public twoPlayerGame(LinearLayout ll){
        if (!initHoney) {
            honey.initialize(6, 20);
            ll.setOrientation(LinearLayout.VERTICAL);
            int toolbarHeight = toolbar.getHeight();
            ll.setPadding(0, toolbarHeight - 10, 0, 0);
            // View bottombar = LayoutInflater.from(MainActivity.context).inflate(R.layout.bottombar, null);
            // ll.addView(bottombar);
            ll.addView(honey);
            initHoney = true;
            flippy.addView(ll, 3);
        }

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flippy.setDisplayedChild(0);
                toolbar.setNavigationIcon(null);
                toolbar.getMenu().clear();
                return;
            }
        });
    }
}
