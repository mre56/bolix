package com.example.matthew.bolix;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;

public class MainActivity extends AppCompatActivity implements HoneycombView.OnCellClickListener {

    protected static ViewFlipper flippy;
    protected static Toolbar toolbar;
    protected static Context context;
    protected static HoneycombView honey;
    protected static boolean initHoney = false;
    protected static int screenWidth;
    protected static int screenHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        flippy = (ViewFlipper) findViewById(R.id.viewFlipper);
        flippy.setDisplayedChild(0);

        context = getApplicationContext();

        DisplayMetrics displaymetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        screenWidth = displaymetrics.widthPixels;
        screenHeight = displaymetrics.heightPixels;

        final Button rulesButton = (Button) flippy.findViewById(R.id.rulesButton);
        final Button twoPlayerButton = (Button) flippy.findViewById(R.id.twoPlayerButton);
        final Button singlePlayerButton = (Button) flippy.findViewById(R.id.singlePlayerButton);
        double w = (int) screenWidth / 2.5;
        twoPlayerButton.setWidth((int) w);
        singlePlayerButton.setWidth((int) w);
        rulesButton.setWidth((int) w);


        twoPlayerButton.setHeight(screenHeight / 8);
        singlePlayerButton.setHeight(screenHeight / 8);
        rulesButton.setHeight(screenHeight / 8);

        double padding = (int) ((screenHeight) / 14);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(0, (int) padding, 0, (int) padding);
        params.gravity = Gravity.CENTER;

        twoPlayerButton.setLayoutParams(params);
        singlePlayerButton.setLayoutParams(params);
        rulesButton.setLayoutParams(params);

        honey = new HoneycombView(this);
        honey.setOnCellClickListener(this);

        rulesButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new Rules();
            }
        });

        twoPlayerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!initHoney) {
                    LinearLayout ll = new LinearLayout(MainActivity.this);
                    new twoPlayerGame(ll);
                }
                flippy.setDisplayedChild(3);
            }
        });

        singlePlayerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
    }

    public void onCellClick(int column, int row) {
        new onCellClick(column, row);
    }
}
