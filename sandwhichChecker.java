package com.example.matthew.bolix;

import android.util.Log;
import java.util.ArrayList;

import static com.example.matthew.bolix.HoneycombView.cellSet;

/**
 * Created by Matthew on 11/12/2016.
 */

public class sandwhichChecker {

    public sandwhichChecker(int col, int row, int newCellColor) {
        int colorInside;
        int colorOutside;
        if(newCellColor == 1) {
            colorInside = 2; // black
            colorOutside = 1; // white
        } else {
            colorInside = 1; // white
            colorOutside = 2; // black
        }
        ArrayList sandwich = onCellClick.getSandwich();
        // Check color above vertical
        if (!(row - 6 < 1)) {
            if (cellSet[col][row - 2] == colorInside) {
                if (cellSet[col][row - 4] == colorInside) {
                    if (cellSet[col][row - 6] == colorOutside) {
                        int[] coordinate_1 = {col, row - 2};
                        int[] coordinate_2 = {col, row - 4};
                        sandwich.add(coordinate_1);
                        sandwich.add(coordinate_2);
                        Log.i("If loop", "true above");
                    }
                }
            }
        }
        // Check color below vertical
        if (!(row + 6 > 19)) {
            if (cellSet[col][row + 2] == colorInside) {
                if (cellSet[col][row + 4] == colorInside) {
                    if (cellSet[col][row + 6] == colorOutside) {
                        int[] coordinate_1 = {col, row + 2};
                        int[] coordinate_2 = {col, row + 4};
                        sandwich.add(coordinate_1);
                        sandwich.add(coordinate_2);
                        Log.i("If loop", "true below");
                    }
                }
            }
        }
        // up left diagonally
        if(row % 2 == 0) { //even
            if (!(col - 2 < 0 || row - 3 < 0)) {
                if (cellSet[col - 1][row - 1] == colorInside) {
                    if (cellSet[col - 1][row - 2] == colorInside) {
                        if (cellSet[col - 2][row - 3] == colorOutside) {
                            int[] coordinate_1 = {col - 1, row - 1};
                            int[] coordinate_2 = {col - 1, row - 2};
                            sandwich.add(coordinate_1);
                            sandwich.add(coordinate_2);
                            Log.i("If loop", "true up left diagonally e");
                        }
                    }
                }
            }
        } else { //odd row
            if (!(col - 1 < 0 || row - 3 < 0)) {
                if (cellSet[col][row - 1] == colorInside) {
                    if (cellSet[col - 1][row - 2] == colorInside) {
                        if (cellSet[col - 1][row - 3] == colorOutside) {
                            int[] coordinate_1 = {col, row - 1};
                            int[] coordinate_2 = {col - 1, row - 2};
                            sandwich.add(coordinate_1);
                            sandwich.add(coordinate_2);
                            Log.i("If loop", "true up left diagonally o");
                        }
                    }
                }
            }
        }

        // down left diagonally
        if(row % 2 == 0) { //even
            if (!(col - 2 < 0 || row + 3 > 19)) {
                if (cellSet[col - 1][row + 1] == colorInside) {
                    if (cellSet[col - 1][row + 2] == colorInside) {
                        if (cellSet[col - 2][row + 3] == colorOutside) {
                            int[] coordinate_1 = {col - 1, row + 1};
                            int[] coordinate_2 = {col - 1, row + 2};
                            sandwich.add(coordinate_1);
                            sandwich.add(coordinate_2);
                            Log.i("If loop", "true down left diagonally e");
                        }
                    }
                }
            }
        } else { //odd row
            if (!(col - 1 < 0 || row + 3 > 19)) {
                if (cellSet[col][row + 1] == colorInside) {
                    if (cellSet[col - 1][row + 2] == colorInside) {
                        if (cellSet[col - 1][row + 3] == colorOutside) {
                            int[] coordinate_1 = {col, row + 1};
                            int[] coordinate_2 = {col - 1, row + 2};
                            sandwich.add(coordinate_1);
                            sandwich.add(coordinate_2);
                            Log.i("If loop", "true down left diagonally o");
                        }
                    }
                }
            }
        }

        // up right diagonally
        if(row % 2 == 0) { //even
            if (!(col + 1 > 5 || row - 3 < 0)) {
                if (cellSet[col][row - 1] == colorInside) {
                    if (cellSet[col + 1][row - 2] == colorInside) {
                        if (cellSet[col + 1][row - 3] == colorOutside) {
                            int[] coordinate_1 = {col, row - 1};
                            int[] coordinate_2 = {col + 1, row - 2};
                            sandwich.add(coordinate_1);
                            sandwich.add(coordinate_2);
                            Log.i("If loop", "true up right diagonally e");
                        }
                    }
                }
            }
        } else { //odd row
            if (!(col + 2 > 5 || row - 3 < 0)) {
                if (cellSet[col + 1][row - 1] == colorInside) {
                    if (cellSet[col + 1][row - 2] == colorInside) {
                        if (cellSet[col + 2][row - 3] == colorOutside) {
                            int[] coordinate_1 = {col + 1, row - 1};
                            int[] coordinate_2 = {col + 1, row - 2};
                            sandwich.add(coordinate_1);
                            sandwich.add(coordinate_2);
                            Log.i("If loop", "true up right diagonally o");
                        }
                    }
                }
            }
        }

        // down right diagonally
        if(row % 2 == 0) { //even
            if (!(col + 1 > 5 || row + 3 > 19)) {
                if (cellSet[col][row + 1] == colorInside) {
                    if (cellSet[col + 1][row + 2] == colorInside) {
                        if (cellSet[col + 1][row + 3] == colorOutside) {
                            int[] coordinate_1 = {col, row + 1};
                            int[] coordinate_2 = {col + 1, row + 2};
                            sandwich.add(coordinate_1);
                            sandwich.add(coordinate_2);
                            Log.i("If loop", "true down right diagonally e");
                        }
                    }
                }
            }
        } else { //odd row
            if (!(col + 2 > 5 || row + 3 > 19)) {
                if (cellSet[col + 1][row + 1] == colorInside) {
                    if (cellSet[col + 1][row + 2] == colorInside) {
                        if (cellSet[col + 2][row + 3] == colorOutside) {
                            int[] coordinate_1 = {col + 1, row + 1};
                            int[] coordinate_2 = {col + 1, row + 2};
                            sandwich.add(coordinate_1);
                            sandwich.add(coordinate_2);
                            Log.i("If loop", "true down right diagonally o");
                        }
                    }
                }
            }
        }
        if(!sandwich.isEmpty()){
            onCellClick.sandwichTurn = true;
        }
    }
}
