package com.example.matthew.bolix;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class HoneycombView extends View {
    private Paint wallPaint = new Paint();
    private Paint fillPaint = new Paint();
    private Paint cachePaint = new Paint();
    private Path combPath;
    private Bitmap cacheBmp;
    private Canvas cacheCan;
    private int cellWidth;
    private int columns;
    private int rows;
    protected static int[][] cellSet; // 0 = gray, 1 = white, 2 = black, 3 = red
    protected static boolean winner = false;

    private OnCellClickListener listener;

    public HoneycombView(Context context) {
        this(context, null);
        this.forceLayout();
    }

    public HoneycombView(Context context, AttributeSet attrs) {
        super(context, attrs);
        wallPaint.setColor(getResources().getColor(R.color.colorPrimary));
        wallPaint.setStyle(Paint.Style.STROKE);
        wallPaint.setStrokeWidth(3f);

        fillPaint.setStyle(Paint.Style.FILL);
        cachePaint.setStyle(Paint.Style.FILL);
    }

    public void initialize(int columns, int rows) {
        this.columns = columns; //6
        this.rows = rows;       //20

        this.cellSet = new int[columns][rows];
        for(int i = 0; i < columns; i++){
            for(int j = 0; j < rows; j++) {
                cellSet[i][j] = 0;
            }
        }
    }

    public interface OnCellClickListener {
        public void onCellClick(int column, int row);
    }

    public void setOnCellClickListener(OnCellClickListener listener)
    {
        this.listener = listener;
    }

    public void setCell(int column, int row, int newCellColor) {
        cellSet[column][row] = newCellColor;
        //checkWin(column, row, newCellColor);
        new winChecker(column, row, newCellColor);
        invalidate();
        if(!winner) {
            new sandwhichChecker(column, row, newCellColor);
            //checkSandwich(column, row, newCellColor);
        }
    }

    public int getCell(int column, int row) {
        return cellSet[column][row];
    }

    public int isCellSet(int column, int row)
    {
        return cellSet[column][row];
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        cacheBmp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        cacheCan = new Canvas(cacheBmp);

        cellWidth = 2 * w / (2 * columns + columns - 1);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(getResources().getColor(R.color.colorAroundBoard));
        boolean oddRow;
        int xOff;

        combPath = getHexPath(cellWidth / 2f, cellWidth / 2f, (float) (cellWidth * Math.sqrt(3) / 4));

        for (int r = 0; r < rows; r++) {
            oddRow =  r % 2 == 1;
            xOff = 0;

            for (int c = 0; c < columns; c++) {
              if( (r == 0) ||
                      (r == 1 && c != 2 ) ||
                      (r == 2 && (c == 0 || c == 1 || c == 4 || c == 5)) ||
                      (r == 3 && (c == 0 || c == 4)) ||
                      (r == 4 && (c == 0 || c == 5)) ||
                      (r == 16 && (c == 0 || c == 5)) ||
                      (r == 17 && (c == 0 || c == 4)) ||
                      (r == 18 && (c == 0 || c == 1 || c == 4 || c == 5)) ||
                      (r == 19 && c != 2 )) {
                  combPath.offset((int) (1.5f * cellWidth), 0);
                  xOff += 1.5f * cellWidth;


              } else {
                if (!(oddRow && c == columns - 1)) {
                    if (cellSet[c][r] == 0) {
                        if(winner){
                            fillPaint.setColor(Color.RED);
                            cellSet[c][r] = 3;
                        } else {
                            fillPaint.setColor(getResources().getColor(R.color.colorEmptySpace));
                        }
                    } else if(cellSet[c][r] == 1){
                        fillPaint.setColor(Color.WHITE);
                    } else if(cellSet[c][r] == 2) {
                        fillPaint.setColor(Color.BLACK);
                    } else if(cellSet[c][r] == 3) {
                        fillPaint.setColor(Color.RED);
                        cellSet[c][r] = 0;
                    }
                    canvas.drawPath(combPath, fillPaint);

                    canvas.drawPath(combPath, wallPaint);

                    cachePaint.setColor(Color.argb(255, 1, c, r));
                    cacheCan.drawPath(combPath, cachePaint);

                    combPath.offset((int) (1.5f * cellWidth), 0);
                    xOff += 1.5f * cellWidth;
                }
              }
            }

            combPath.offset(-xOff + (oddRow ? -1 : 1) * 3 * cellWidth / 4, (float) (cellWidth * Math.sqrt(3) / 4));
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != MotionEvent.ACTION_DOWN)
            return true;

        int pixel = cacheBmp.getPixel((int) event.getX(), (int) event.getY());

        int r = Color.red(pixel);
        if (r == 1) {
            int g = Color.green(pixel);
            int b = Color.blue(pixel);

            Log.i("cellset", g + " " + b + " color:" + cellSet[g][b]);

            if (listener != null) {
                listener.onCellClick(g, b);
            }
        }
        return true;
    }

    private Path getHexPath(float size, float centerX, float centerY) {
        Path path = new Path();

        for (int j = 0; j <= 6; j++) {
            double angle = j * Math.PI / 3;
            float x = (float) (centerX + size * Math.cos(angle));
            float y = (float) (centerY + size * Math.sin(angle));
            if (j == 0) {
                path.moveTo(x, y);
            }
            else {
                path.lineTo(x, y);
            }
        }

        return path;
    }
}