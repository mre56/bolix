# README #

I have this board game. I like it. There wasn't an app. I made one.

### Bolix Android App ###
* Bolix(also known as Boku, Bollox, five-in-a-row) is a very simple to learn and play. It only has two rules. 
* rule 1 the game is won by putting five marbles into a row
* rule 2 if a player traps two of his opponent's marbles between two of his own, he may remove one of the sandwiched marbles (and the opponent may not put a marble back into the same place with his next move
* Here's the wiki https://en.wikipedia.org/wiki/B%C5%8Dku
* I use this for a rules page: http://boku.bandoodle.co.uk/rules.php
* Version 0.1