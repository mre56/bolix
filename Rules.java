package com.example.matthew.bolix;

import android.view.View;

/**
 * Created by Matthew on 11/8/2016.
 */

public class Rules {
    public Rules() {
        MainActivity.flippy.setDisplayedChild(1);
        MainActivity.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        MainActivity.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.flippy.setDisplayedChild(0);
                MainActivity.toolbar.setNavigationIcon(null);
                MainActivity.toolbar.getMenu().clear();
                return;
            }
        });
    }
}
